


$(document).ready(function (e) {
    document.getElementById('toifa').addEventListener('change', e => {
        var toifa = $("#toifa").find(":selected").text();
     
        $.ajax({
            url: '/base_app/davomat/',
            type: 'post',
            data: {
                id: e.target.value,
                select: 'toifa',
                csrfmiddlewaretoken: $('#csrf_token').val(),
            },
            'Content-type': 'application/json',
            success: function (response) {

                $('#guruh').empty();
                $('#guruh').append('<option></option>')
                $.each(response['gs'], function (i, v) {
                    $('#guruh').append('<option value="' + v['id'] + '">' + v["guruh"] + '</option>')
                })


            },
            error: function (res) {
                console.log(res)
            }
        });
    })

    document.getElementById('btn_davomat').addEventListener('click', e => {
        var toifa = $('#toifa').val();
        if (toifa) {



            var guruh = $('#guruh').val();
            $.ajax({
                url: '/base_app/davomat/',
                type: 'post',
                data: {
                    guruh: $('#guruh').val(),
                    toifa: $('#toifa').val(),
                    soat: $('#soat').val(),
                    select: 'guruh',
                    csrfmiddlewaretoken: $('#csrf_token').val(),
                },
                'Content-type': 'application/json',
                success: function (response) {
                    // console.log(response['s']);
                    $('#customers').empty();
                    $('#customers').append('<tr class="bosh"><th rowspan="2" style="width: 5%;">№</th><th rowspan="2">FISH</th><th rowspan="2">Ta\'lim muassasining nomi</th><th colspan="2"><input id="sana"  name="datemax" type="date"></th></tr><tr><th>Davomat</th><th>Faolik darajasi</th></tr>')


                    $.each(response['s'], function (i, v) {
                        n = i + 1
                        $('#customers').append('<tr class="rm"><td>' + n + '</td><td class="ism">' + v["fish"] + '</td><td class="joy">' + v["joy"] + '</td>  <td id = "foscheck" class="foscheck"><input type="checkbox"  class="check"  name="' + n + '" id="' + n + '"><label for="' + n + '"></label></td> <td class="select"><select id="activity' + n + '" name="activity"><option value="passiv-' + v['id'] + '">passiv</option><option value="faol-' + v['id'] + '">faol</option><option value="yaxshi-' + v['id'] + '">yaxshi</option></select></td></tr>')


                    });

                },
                error: function (res) {
                    console.log(res);
                }
            });
        }
        else {
            swal("Toifani tanlamadingiz ", "iltimos toifani tanlang", "error")
        }

    })


    var click = 0;
    document.getElementById('save_btn').addEventListener('click', e => {
        // var conceptName = $('#aioConceptName').find(":selected").text();

       
        var n = $(".select").length;
        var faol = [];
        for (var i = 1; i <= n; i++) {
            faol.push($("#activity" + i + "").find(":selected").val());
        }
      
        check_id = [];
        var check = $(".check").length;
        for (var i = 1; i <= check; i++) {
            check_id.push($(".check#" + i + "").is(":checked"));
        }
       
        var modul_id = document.getElementById("modul").value;
        var sana = document.getElementById("sana").value;
        if (sana==''){
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        
        today =yyyy + '-' + mm + '-' + dd;
        sana=today
        }
        var mavzu = document.getElementById("mavzu").value;
        var toifa_id = document.getElementById("toifa").value;
        var guruh_id = document.getElementById("guruh").value;
        var soat = document.getElementById('soat').value;
        if (click < 1) {
            if (faol.length != 0) {
                if (mavzu) {
                    if (modul_id) {


                        $.ajax({
                            url: '/base_app/dars/',
                            type: 'post',
                            data: {

                                'faol': faol,
                                'check_id': check_id,
                                'modul_id': modul_id,
                                'toifa_id': toifa_id,
                                'guruh_id': guruh_id,
                                'mavzu': mavzu,
                                'soat': soat,
                                'click': click,
                                'sana': sana,
                                csrfmiddlewaretoken: $('#csrf_token').val(),
                            },
                            'Content-type': 'application/json',
                            success: function (res) {
                                console.log(res)
                                if (res['status'] == 'ok') {
                                    swal("Bugungi dars bo'yicha ma'lumotlar saqlandi", "Malumotlar saqlandi", "success")
                                }
                                click += 1;
                            },
                            error: function (err) {
                                console.log(err);
                            }
                        })
                    }
                    else {
                        swal(" Modulni tanlamadingiz! ", "iltimos modulni tanlang ", "error")
                    }

                }
                else {
                    swal(" Mavzuni kiritmadingiz! ", "iltimos mavzuni kiriting", "error")
                }
            }
            else {
                swal("<>Qidirish<> tugmasini bosing tinglovchilar ro'yxati chiqadi ", "iltimos tugmani bosingg", "error")
            }
        }
        else {
            swal("Bugungi ma'lumotlar saqlandi! ", "Endi o'zgarish kirita olmaysiz", "error")
        }

    })



})
function refresh() {
    window.location.reload("Refresh")
}
