import json
from django.contrib import messages
from .forms import CustomUserForm

from django.shortcuts import render, HttpResponse, redirect
from django.contrib.auth import authenticate, login, logout


def SignupView(request):
    form = CustomUserForm()
    if request.method == 'POST':
        form = CustomUserForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            user = form.cleaned_data.get('username')
            messages.success(request, 'Account was created for ' + user)
            return render(request, 'index.html')
    return render(request, 'index.html', {'form': form})


def LoginPage(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user:
            login(request, user)
            return render(request, 'index.html')

        else:
            messages.info(request, "Login yoki parol xato")
            return render(request, 'index.html', {'incorrect': True})

    context = {}
    return render(request, 'index.html', context)


def Logout(request):
    logout(request)
    return redirect('accounts:signup')


def indexView(request):
    return redirect('accounts:signup')
