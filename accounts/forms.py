from django.forms import ModelForm
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import MyUser





class CustomUserForm(UserCreationForm):
    class Meta:
        model = MyUser
        fields = ['username', 'first_name', 'last_name', 'email', 'password1', 'password2', 'photo', 'phone', 'ilmiy_daraja','kim']

class CustomUserChangeForm(UserChangeForm):
    class Meta:
        model = MyUser
        fields = ['username', 'first_name', 'last_name', 'email','photo', 'phone', 'ilmiy_daraja','kim']