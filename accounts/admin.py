from django import forms
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from accounts.forms import CustomUserForm, CustomUserChangeForm
from accounts.models import MyUser,Kim


class CustomUserAdmin(UserAdmin):
    add_form = CustomUserForm
    form = CustomUserChangeForm
    model = MyUser
    list_display = ['username', 'first_name', 'last_name', 'email','password','photo', 'phone', 'ilmiy_daraja','kim']
    fieldsets = UserAdmin.fieldsets + (
        (None, {'fields': ('photo', 'phone', 'ilmiy_daraja','kim')}),
    )
    add_fieldsets = UserAdmin.add_fieldsets + (
        (None, {'fields': ('photo', 'phone', 'ilmiy_daraja','kim')}),
    )


admin.site.register(MyUser, CustomUserAdmin)
admin.site.register(Kim)
