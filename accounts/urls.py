from django.urls import path
from . import views

app_name = 'accounts'

urlpatterns = [
    path('', views.indexView, name='index'),
    path('signup/', views.SignupView, name="signup"),
    path('login/', views.LoginPage, name="login"),
    path('logout/', views.Logout, name="logout"),



    # path('viloyat/', views.ViloyatView, name='viloyat'),

]
