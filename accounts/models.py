from django.db import models
from django.contrib.auth.models import AbstractUser
      


class Kim(models.Model):
    kim = models.CharField(max_length=50)

    def __str__(self):
        return self.kim


class MyUser(AbstractUser):
    # email = models.EmailField(max_length=50, blank=True, null=True)
    photo = models.ImageField(upload_to='images/',blank=True, null=True)
    phone = models.CharField(max_length=25)
    ilmiy_daraja = models.CharField(max_length=100)
    kim = models.ForeignKey(Kim, on_delete=models.CASCADE, null=True, blank=True)