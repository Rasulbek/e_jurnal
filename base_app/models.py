from enum import unique
from django.db import models
from django.db.models.base import Model
from django.db.models.deletion import CASCADE
from accounts.models import MyUser
import datetime


class MO_yili(models.Model):
    yil = models.DateField()

    def __str__(self):
        return str(self.yil)


class MO_oyi(models.Model):
    oy = models.CharField(max_length=250, verbose_name='MO oyi')

    def __str__(self):
        return self.oy


class Viloyat(models.Model):
    name = models.CharField(
        max_length=250, verbose_name="Viloyat nomi", unique=True)

    def __str__(self):
        return self.name


class Tuman(models.Model):
    viloyat_id = models.ForeignKey(Viloyat, on_delete=models.CASCADE)
    name = models.CharField(
        max_length=250, verbose_name="Tuman (shahar)", unique=True)

    def __str__(self):
        return self.name


class Toifa(models.Model):
    toifa = models.CharField(max_length=250, verbose_name="Toifa", unique=True)

    def __str__(self):
        return str(self.toifa)


class Ilmiy_daraja(models.Model):
    daraja = models.CharField(max_length=250, null=True, blank=True)

    def __str__(self):
        return str(self.id)


class Shakl(models.Model):
    shakl = models.CharField(max_length=15)

    def __str__(self):
        return self.shakl


class Guruh(models.Model):
    toifa = models.ForeignKey(
        Toifa, on_delete=models.SET_NULL, null=True, blank=True)
    guruh = models.CharField(max_length=250, null=True,
                             blank=True, unique=True)

    def __str__(self):
        return str(self.guruh)


class Modul(models.Model):
    modul = models.CharField(max_length=250)

    def __str__(self):
        return self.modul


class Tinglovchi(models.Model):
    fish = models.CharField(max_length=250)
    yil = models.DateField(verbose_name="MO oyi", auto_now_add=True)
    oy = models.DateField(verbose_name="MO oyi", auto_now_add=True)
    # shakl = models.ForeignKey(Shakl, on_delete=models.CASCADE)
    viloyat = models.ForeignKey(
        Viloyat, on_delete=models.CASCADE, verbose_name="Viloyat")
    # tuman = models.ForeignKey(Tuman, on_delete=models.CASCADE, verbose_name="Tuman(shahar)")
    joy = models.CharField(
        max_length=250, verbose_name="Ta'lim muassasasi nomi")
    tell = models.CharField(max_length=15, verbose_name="Telefon nomer")
    email = models.EmailField(verbose_name="Email", null=True, blank=True)
    toifa = models.ForeignKey(
        Toifa, on_delete=models.CASCADE, verbose_name="Toifa")
    guruh = models.ForeignKey(
        Guruh, on_delete=models.CASCADE, null=True, blank=True)
    # daraja = models.ForeignKey(Ilmiy_daraja, on_delete=models.CASCADE)
    # himoya_yil = models.DateField(null=True, blank=True)

    def __str__(self):
        if self.fish:
            return f'{self.fish}'
        return f'{self.id}'


class Monitoring(models.Model):
    tinglovchi = models.ForeignKey(Tinglovchi, on_delete=models.CASCADE)
    kirish_test = models.CharField(max_length=3, null=True, blank=True)
    chiqish_test = models.CharField(max_length=3, null=True, blank=True)
    kirish_sana = models.DateField(null=True, blank=True)
    chiqish_sana = models.DateField(null=True, blank=True)

    def __str__(self):
        return str(self.tinglovchi)


class Dars(models.Model):
    active = [
        ('passiv', "Passiv"),
        ('yaxshi', 'Yaxshi'),
        ('faol', 'Faol')
    ]
    oqituvchi = models.ForeignKey(MyUser, on_delete=models.CASCADE)
    tinglovchi = models.ForeignKey(Tinglovchi, on_delete=models.CASCADE)
    mavzu = models.CharField(max_length=250)
    modul = models.ForeignKey(Modul, on_delete=models.CASCADE)
    sana = models.DateField(default=datetime.datetime.today())
    toifa = models.ForeignKey(Toifa, on_delete=models.CASCADE)
    guruh = models.ForeignKey(
        Guruh, on_delete=models.CASCADE, null=True, blank=True)
    soat = models.PositiveIntegerField()
    is_absent = models.CharField(default="+", max_length=10)
    is_active = models.CharField(max_length=50, choices=active)

    def __str__(self):
        return str(self.tinglovchi)


class Bitiruv(models.Model):
    toifa = models.ForeignKey(Toifa, on_delete=models.CASCADE)
    guruh = models.ForeignKey(
        Guruh, on_delete=models.CASCADE, null=True, blank=True, default=None)
    tinglovchi = models.ForeignKey(Tinglovchi, on_delete=models.CASCADE)
    bmi_mavzu = models.CharField(max_length=250)
    bmi_rahbar = models.CharField(max_length=250)
    bmi_ball = models.PositiveIntegerField()
    file = models.FileField(upload_to='bitiruv/')
    monitoring = models.ForeignKey(
        Monitoring, on_delete=models.CASCADE, null=True, blank=True)
    sana = models.DateField(auto_now_add=True)

    # class Meta:
    #     unique_together = ('tinglovchi',)
