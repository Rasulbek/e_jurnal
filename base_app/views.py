import datetime
from django.forms.forms import Form
import pytz
import json
import re
import urllib
import xlwt

from tablib import Dataset
from django.contrib.auth.mixins import LoginRequiredMixin

from django.http.response import HttpResponse

from .resources import TinglovchiResource
from django.http import JsonResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.contrib import messages
from django.views.generic import TemplateView, CreateView, ListView
from . import models
from .forms import TinglovchiForm, BitiruvForm

from django.contrib.auth.decorators import login_required, user_passes_test

from accounts.models import Kim, MyUser
from import_export.admin import ImportExportModelAdmin
from django.db.models import Count
from base_app import resources
from django.utils.timezone import utc
from django.db.models import Q


class MonitoringView(LoginRequiredMixin, ListView):
    template_name = 'monitoring.html'
    model = models.Toifa

    def post(self, request):
        if request.is_ajax() and request.method == "POST":
            if request.POST.get('select') == 'toifa':
                toifa = request.POST['toifa']
                try:
                    guruh = list(models.Toifa.objects.get(
                        toifa=toifa).guruh_set.all().values())
                except:
                    pass
                return JsonResponse({'guruh': guruh})
            elif request.POST.get('select') == 'search':
                toifa = request.POST['toifa']
                guruh = request.POST['guruh']
                try:
                    guruh = models.Guruh.objects.get(guruh=guruh)
                except:
                    pass
                if toifa == 'Guruh rahbari':

                    start_date = datetime.datetime.now().date()
                    end_date = start_date + datetime.timedelta(days=45)

                    tinglovchi1 = guruh.tinglovchi_set.filter(
                        oy__lte=end_date).values()
                    tinglovchi2 = guruh.tinglovchi_set.filter(
                        oy__gte=start_date).values()
                    tinglovchi = list(tinglovchi1.union(tinglovchi2))

                    return JsonResponse({"ting": tinglovchi})
                else:
                    tinglovchi = list(models.Toifa.objects.get(toifa=toifa).tinglovchi_set.filter(
                        oy__month=datetime.datetime.now().month,
                        yil__year=datetime.datetime.now().year).values())
                    return JsonResponse({"ting": tinglovchi})

            elif request.POST.get('select') == 'test':
                kirish_test = request.POST.getlist('kirish[]')
                chiqish_test = request.POST.getlist('chiqish[]')
                ting_id = request.POST.getlist('id_ting[]')
                guruh = request.POST.get('guruh')

                for i in ting_id:
                    m = models.Monitoring()
                    ind = ting_id.index(i)
                    m.tinglovchi = models.Tinglovchi.objects.get(id=i)
                    m.kirish_test = kirish_test[ind]
                    m.chiqish_test = chiqish_test[ind]
            

                    m.save()

                return JsonResponse({'statuc': 'ok'})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['admin'] = MyUser.objects.filter(username=self.request.user)
        return context


class MalakaView(LoginRequiredMixin, ListView):
    template_name = 'malaka.html'
    model = models.Tinglovchi

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['toifalar'] = models.Toifa.objects.all()
        context['ting'] = models.Tinglovchi.objects.filter(oy__month=datetime.datetime.now().month,
                                                           yil__year=datetime.datetime.now().year)

        context['soni'] = models.Tinglovchi.objects.values(
            'toifa').annotate(soni=Count('toifa'))
        context['ting'] = models.Tinglovchi.objects.filter(oy__month=datetime.datetime.now().month,
                                                           yil__year=datetime.datetime.now().year)

        return context


@login_required()
def BitiruvView(request):
    form = BitiruvForm()
    if request.method == 'POST' and request.is_ajax():
        if request.POST.get('select') == 'toifa':
            toifa = request.POST.get('toifa')

            if toifa == 'Guruh rahbari':
                guruh = list(models.Toifa.objects.get(
                    toifa=toifa).guruh_set.all().values())
                return JsonResponse({'guruh': guruh})
            else:
                ting = list(
                    models.Toifa.objects.get(toifa=toifa).tinglovchi_set.filter(oy__month=datetime.datetime.now().month,
                                                                                yil__year=datetime.datetime.now().year).values())
                return JsonResponse({'ting': ting})
        elif request.POST.get('select') == 'guruh':
            guruh = request.POST.get('guruh')
            guruh = models.Guruh.objects.get(guruh=guruh)
            start_date = datetime.datetime.now().date()
            end_date = start_date + datetime.timedelta(days=45)

            tinglovchi1 = guruh.tinglovchi_set.filter(
                oy__lte=end_date).values()
            tinglovchi2 = guruh.tinglovchi_set.filter(
                oy__gte=start_date).values()
            ting = list(tinglovchi1.union(tinglovchi2))

            return JsonResponse({'ting': ting})

        elif request.POST.get('select') == 'data':
            toifa = request.POST.get('toifa')
            guruh = request.POST.get('guruh')
            tinglovchi = request.POST.get('tinglovchi')
            mavzu = request.POST.get('mavzu')
            rahbar = request.POST.get('rahbar')
            ball = request.POST.get('ball')
            file = request.POST.get('file')

            b = models.Bitiruv()

            b.toifa = models.Toifa.objects.get(id=toifa)

            try:
                b.guruh = models.Guruh.objects.get(id=guruh)
            except:
                b.guruh = None

            b.tinglovchi = models.Tinglovchi.objects.get(id=tinglovchi)

            b.bmi_mavzu = mavzu
            b.bmi_rahbar = rahbar
            b.bmi_ball = ball
            b.file = file
            try:
                b.monitoring = models.Monitoring.objects.get(
                    tinglovchi_id=tinglovchi)
            except:
                return JsonResponse({'status': 'xato'})


         


            b.save()
            return JsonResponse({'status': 'ok'})
    context = {
        'form': form,
    }
    return render(request, 'bitiruv.html', context)


# def is_dekan(user):
#     try:
#         k = Kim.objects.get(kim='dekanat')
#         return user.is_authenticated and user.kim.kim == 'dekanat'
#     except:
#         return False
# @user_passes_test(is_dekan)
@login_required()
def TinglovchiView(request):
    form = TinglovchiForm()
    if request.method == 'POST':
        form = TinglovchiForm(request.POST)
        if form.is_valid():
            form.save()
            user = form.cleaned_data.get('ism')
            messages.success(request, 'ok')
            return redirect('base_app:tinglovchi')
        else:
            pass

    data = {
        'form': form,
    }
    return render(request, 'ting.html', context=data)


def ViloyatView(request):
    if request.method == 'POST':
        viloyat = request.POST["vil"]
        tuman = list(models.Viloyat.objects.get(
            id=viloyat).tuman_set.all().values())
        return JsonResponse({'t': tuman})


def ToifaView(request):
    if request.method == 'POST':
        toifa = request.POST["toifa"]
        guruh = list(models.Toifa.objects.get(
            id=toifa).guruh_set.all().values())
        return JsonResponse({'guruh': guruh})


# def is_teacher(user):
#     try:
#         k = Kim.objects.get(kim='O\'qituvchi')
#         return user.is_authenticated and user.kim.kim == 'O\'qituvchi'
#     except:
#         return False
#
#
# @user_passes_test(is_teacher)

@login_required()
def DavomatView(request):
    oqituvchi = models.MyUser.objects.all()
    toifa = models.Toifa.objects.all()
    guruh = models.Guruh.objects.all()
    modul = models.Modul.objects.all()
    admin = MyUser.objects.filter(username=request.user)
    if request.method == 'POST':
        if request.POST.get('select') == 'toifa':
            try:
                toifa = models.Toifa.objects.get(id=request.POST.get('id'))
            except:
                return JsonResponse({'gs': [], })

            guruhs = list(toifa.guruh_set.all().values())
            return JsonResponse({'gs': guruhs})
        elif request.POST.get('select') == 'guruh':
            try:
                guruh = models.Guruh.objects.get(id=request.POST.get('guruh'))

            except:
                toifa = models.Toifa.objects.get(id=request.POST.get('toifa'))
                students = list(toifa.tinglovchi_set.filter(oy__month=datetime.datetime.now().month,
                                                            yil__year=datetime.datetime.now().year).values())
                return JsonResponse({'s': students})
            start_date = datetime.datetime.now().date()
            end_date = start_date + datetime.timedelta(days=45)

            students1 = guruh.tinglovchi_set.filter(oy__lte=end_date).values()
            students2 = guruh.tinglovchi_set.filter(
                oy__gte=start_date).values()
            students = list(students1.union(students2))

            return JsonResponse({'s': students})

    context = {
        "oqituvchi": oqituvchi,
        "toifa": toifa,
        "guruh": guruh,
        "modul": modul,
        "admin": admin
    }
    return render(request, 'davomat.html', context)


def attandance(request):
    if request.method == 'POST':
        return redirect('base_app:davomat')


@login_required()
def DarsView(request):
    if request.is_ajax() and request.method == 'POST':
        click = request.POST.get('click')
        faol = request.POST.getlist('faol[]')
        absent = request.POST.getlist('check_id[]')

        for i in faol:
            ind = faol.index(i)
            a, id = re.findall(r'(\w+)-(\d+)', i)[0]

            dars = models.Dars()
            dars.oqituvchi = request.user
            dars.mavzu = request.POST.get('mavzu')
            dars.soat = request.POST.get('soat')
            try:
                dars.sana = request.POST.get('sana')
               
            except:
                dars.sana = datetime.datetime.today
            dars.modul = models.Modul.objects.get(
                modul=request.POST.get('modul_id'))
            dars.toifa = models.Toifa.objects.get(
                id=request.POST.get('toifa_id'))
            try:
                dars.guruh = models.Guruh.objects.get(
                    id=request.POST.get('guruh_id'))
            
            except:
                dars.guruh = None

            dars.tinglovchi = models.Tinglovchi.objects.get(id=id)
            dars.is_active = a
            dars.is_absent = convert_boolean(absent[ind])
            
            dars.save()
            
              
    return JsonResponse({'status': "ok"})


def convert_boolean(s):

  
    if s.lower() == 'false':
        return "+"
    elif s.lower() == 'true':
        return "-"


def export_users_xls(request):
    response = HttpResponse(content_type='text/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="Restr.xls"'

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Users', cell_overwrite_ok=True)

    # Sheet header, first row
    row_num = 0
    row_n = 1

    font_style = xlwt.XFStyle()
    font_style.font.bold = True

    columns = ['MO yili', 'MO oyi', 'Dars o\'tilgan sana', 'Toifa', 'Guruh raqami', 'FISH', 'Ish joyi',
               'O\'tilayotgan modul', 'soati', 'Mazkur moduldan dars o\'tgan o\'qituvchi', 'Darsga qatnashmaganlar',
               'Tinglovchining darsda faolligi']

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    # Sheet body, remaining rows
    font_style = xlwt.XFStyle()

    rows = models.Dars.objects.filter(sana=datetime.date.today()).values_list('tinglovchi_id__yil', 'tinglovchi_id__oy',
                                                                              'sana', 'toifa__toifa',
                                                                              'tinglovchi_id__guruh__guruh',
                                                                              'tinglovchi_id__fish',
                                                                              'tinglovchi_id__joy', 'modul__modul',
                                                                              'soat', 'oqituvchi__first_name',
                                                                              'is_absent', 'is_active')
    rows = [[x.strftime("%Y-%m-%d") if isinstance(x,
                                                  datetime.datetime) else x for x in row] for row in rows]
    for row in rows:
        row_num += 1
        for col_num in range(len(row)):
            ws.write(row_num, col_num, row[col_num], font_style)
    wb.save(response)
    return response


def export_yakun(request):
    response = HttpResponse(content_type='text/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="Restr.xls"'

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Users', cell_overwrite_ok=True)

    # Sheet header, first row
    row_num = 0
    row_n = 1

    font_style = xlwt.XFStyle()
    font_style.font.bold = True

    columns = ['FISH', 'Chiqish test', 'BMI mavzusi', 'BMI rahbari', 'BMI bali', 'Sertifikatga tavsiya',
               'Ting saf chaqirilganligi']

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    font_style = xlwt.XFStyle()

    rows = models.Bitiruv.objects.all().values_list('tinglovchi_id__fish', 'monitoring_id__chiqish_test', 'bmi_mavzu',
                                                    'bmi_rahbar', 'bmi_ball', )
    rows = [[x.strftime("%Y-%m-%d") if isinstance(x,
                                                  datetime.datetime) else x for x in row] for row in rows]
    for row in rows:
        row_num += 1
        for col_num in range(len(row)):
            ws.write(row_num, col_num, row[col_num], font_style)

           


    wb.save(response)
    return response


def simple_upload(request):
    if request.method == 'POST':
        ting_resource = TinglovchiResource()
        dataset = Dataset()
        new_persons = request.FILES['myfile']

        imported_data = dataset.load(new_persons.read())
        result = ting_resource.import_data(
            dataset, dry_run=True)  # Test the data import

      
        if not result.has_errors():

            ting_resource.import_data(
                dataset, dry_run=False)  # Actually import now

    return render(request, 'ting.html')


def indexView(request):
    return render(request, 'index.html')
