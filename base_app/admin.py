from django.contrib import admin
from .models import *
from import_export import resources
from import_export.admin import ImportExportModelAdmin
from .resources import TinglovchiResource, DarsResource, ModulResource


class ImportTinglovchi(ImportExportModelAdmin):
    resource_class = TinglovchiResource
    search_fields = ['fish', ]


class ImportModul(ImportExportModelAdmin):
    resources_class = ModulResource


class ImportDars(ImportExportModelAdmin):
    resource_class = DarsResource
    search_fields = ['sana']


admin.site.register(Toifa)
admin.site.register(Tuman)
admin.site.register(Viloyat)
admin.site.register(MO_oyi)
admin.site.register(MO_yili)
admin.site.register(Modul, ImportModul)
admin.site.register(Guruh)
admin.site.register(Dars, ImportDars)
admin.site.register(Tinglovchi, ImportTinglovchi)
admin.site.register(Shakl)
admin.site.register(Monitoring)
admin.site.register(Bitiruv)
admin.site.register(Ilmiy_daraja)
