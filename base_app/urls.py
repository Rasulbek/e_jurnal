from django.urls import path
from . import views

app_name = 'base_app'

urlpatterns = [


    path('tinglovchi/', views.TinglovchiView, name='tinglovchi'),
    # path('tinglovchi/', views.TinglovchiView.as_view(), name='tinglovchi'),

    path('davomat/', views.DavomatView, name='davomat'),
    path('attandance/', views.attandance, name='attandance'),
    path('viloyat/', views.ViloyatView, name='viloyat'),
    path('toifa/', views.ToifaView, name='toifa'),
    path('dars/', views.DarsView, name='dars'),
    path('monitoring/', views.MonitoringView.as_view(), name="monitoring"),
    path('malaka/', views.MalakaView.as_view(), name="malaka"),
    path('bitiruv/', views.BitiruvView, name="bitiruv"),
    path('simple_upload/', views.simple_upload, name='simple_upload'),
    path('yakun/', views.export_yakun, name='yakun'),
    
    path('export/xlsx/$', views.export_users_xls, name='export_users_xlsx'),
]

