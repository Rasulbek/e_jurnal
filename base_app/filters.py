import django_filters

from .models import *


class DarsFilter(django_filters.FilterSet):
    class Meta:
        model = Dars
        fields = '__all__'
