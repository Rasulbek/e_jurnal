from django.forms import ModelForm
from django.contrib.auth.forms import UserCreationForm
from .models import Bitiruv, Tinglovchi, Dars


class TinglovchiForm(ModelForm):
    class Meta():
        model = Tinglovchi
        fields = '__all__'


class BitiruvForm(ModelForm):
    class Meta():
        model = Bitiruv
        fields = '__all__'
