from import_export import resources, fields
from import_export import widgets
from import_export.widgets import ForeignKeyWidget, Widget

import tablib
from collections import OrderedDict

from .models import *
import datetime

from django.utils.timezone import utc

now = datetime.datetime.utcnow().replace(tzinfo=utc)


class TinglovchiResource(resources.ModelResource):
    fish = fields.Field(column_name='fish', attribute='fish')
    yil = fields.Field(column_name='MO yili', attribute='yil')
    oy = fields.Field(column_name='MO oyi', attribute='oy')
    viloyat = fields.Field(column_name='viloyat', attribute='viloyat', widget=ForeignKeyWidget(Viloyat, 'name'))
    joy = fields.Field(column_name='joy', attribute='joy')
    tell = fields.Field(column_name='tell', attribute='tell')
    email = fields.Field(column_name='email', attribute='email')
    toifa = fields.Field(column_name='Toifa', attribute='toifa', widget=ForeignKeyWidget(Toifa, 'toifa'))
    guruh = fields.Field(column_name='Guruh', attribute='guruh', widget=ForeignKeyWidget(Guruh, 'guruh'))

    class Meta:
        model = Tinglovchi
        fields = (
            'id', 'fish', 'yil', 'oy', 'shakl', 'viloyat','joy', 'tell', 'email', 'toifa', 'guruh',)
        use_bulk = True

    def before_import_row(self, row, row_number=None, **kwargs):
        # print('ROW... ', row.get('MO yili'))

        # yil = datetime.datetime.strptime(str(row.get('MO yili')), '%Y-%m-%d %H:%M:%S')
        # oy = datetime.datetime.strptime(str(row.get('MO oyi')), '%Y-%m-%d %H:%M:%S')
        toifa = row.get('Toifa')
        toifa, created = Toifa.objects.get_or_create(toifa=toifa)
        guruh = row.get('Guruh')
        guruh, created = Guruh.objects.get_or_create(guruh=guruh, toifa=toifa)
        # shakl = row.get('MO shakli')
        # shakl = Shakl.objects.get_or_create(shakl=shakl)

        viloyat = row.get('viloyat')
        viloyat, created = Viloyat.objects.get_or_create(name=viloyat)

        # tuman = row.get('tuman')
        # Tuman.objects.get_or_create(name=tuman, viloyat_id=viloyat)

        return super().before_import_row(row, row_number=row_number, **kwargs)


# class FullNameForeignKeyWidget(ForeignKeyWidget):
   
#     def get_queryset(self, value, row):
#         return self.MyUser.objects.filter(
#             first_name__iexact=row["first_name"],
#             username__iexact=row["username"],
#             last_name__iexact=row["last_name"]
#         )

class FullNameForeignKeyWidget(ForeignKeyWidget):
    def get_queryset(self, value, row):
        return self.MyUser.objects.filter(
            first_name__iexact=row["first_name"],
            username__iexact=row["username"],
            last_name__iexact=row["last_name"]
        )


class DarsResource(resources.ModelResource):
    oqituvchi = fields.Field(column_name='O\'qituvchi', attribute='oqituvchi',
                             widget=ForeignKeyWidget(MyUser, 'first_name'))
    tinglovchi = fields.Field(column_name='Tinglovchi', attribute='tinglovchi',
                              widget=ForeignKeyWidget(Tinglovchi, 'fish'))
    mavzu = fields.Field(column_name='Mavzu', attribute='mavzu')
    modul = fields.Field(column_name='Modul', attribute='modul', widget=ForeignKeyWidget(Modul, 'modul'))
    sana = fields.Field(column_name='Sana', attribute='sana')
    toifa = fields.Field(column_name='Toifa', attribute='toifa', widget=ForeignKeyWidget(Toifa, 'toifa'))
    guruh = fields.Field(column_name='Guruh', attribute='guruh', widget=ForeignKeyWidget(Guruh, 'guruh'))
    soat = fields.Field(column_name='Soat', attribute='soat')
    is_absent = fields.Field(column_name='Davomat', attribute='is_absent')
    is_active = fields.Field(column_name='Aktivlik', attribute='is_active')

    class Meta:
        model = Dars
    #     fields = (
    #         'id', 'fish', 'yil', 'oy', 'shakl', 'viloyat', 'tuman', 'joy', 'tell', 'email', 'toifa', 'guruh', 'daraja',
    #         'himoya_yil')
    #     use_bulk = True


class ModulResource(resources.ModelResource):
    modul = fields.Field(column_name='Modul', attribute='modul')

    class Meta:
        model = Modul
